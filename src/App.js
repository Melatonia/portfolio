import React from 'react';
import ProfilePhoto from './assets/Profilephoto.png'
import './App.css';
import { TiUser, TiContacts, TiHome, TiPhone, TiMail, TiSpanner, TiBriefcase, TiClipboard, TiPencil } from "react-icons/ti";

class App extends React.Component {
  render() {
    return <div className='container'>
      <div className='personal'>
        <div className='heading'> <img src={ProfilePhoto} />
          <h2>PRESLAVA DIMITROVA PETKOVA</h2>
        </div>
        <div className='subheading'><TiContacts /> <span>CONTACT</span></div>
        <ul>
          <li><TiPhone /> <p>(+359) 898 551 132 </p></li>
          <li><TiMail /> <p className='email'>preslava.dpetkova @gmail.com</p> </li>
          <li><TiHome /> <p>Albena Hills, Osenovo, Varna, Bulgaria</p></li>
        </ul>
        <div className='subheading'><TiSpanner /> <span>SKILLS</span></div>
        <div className='skills'>
          <span>HTML</span>
          <span>CSS</span>
          <span>JavaScript</span>
          <span>React</span>
          <span>Git</span>
        </div>
        <div className='subheading'><TiClipboard /> <span>CERTIFICATES</span></div>
        <ul className='certificates'>
          <li>React For Beginners</li>
          <li>Programming Basics with Java</li>
          <li>IELTS Certificate</li>
        </ul>
      </div>
      <div className='education-and-work'>
        <hr />
        <div className='education'>
          <TiPencil /> <h3>EDUCATION</h3>
          <ul>
            <li>
              <span className='title'>SELF-STUDY</span> <br />
              <span className='location'>NA</span> <br />
              <span className='date'>July 2019 - September 2019</span> <br />
            </li>
            <li>
              <span className='title'>REACT FOR BEGINNERS COURSE</span> <br />
              <span className='location'>Online</span> <br />
              <span className='date'>February 2019 - June 2019</span> <br />
            </li>
            <li>
              <span className='title'>TECHNOLOGY FUNDAMENTALS</span> <br />
              <span className='location'>Software University, Sofia, Bulgaria</span> <br />
              <span className='date'>September 2018 - January 2019</span> <br />
            </li>
            <li>
              <span className='title'>PROGRAMMING BASICS WITH JAVA</span> <br />
              <span className='location'>Software University, Sofia, Bulgaria</span> <br />
              <span className='date'>April 2018 - June 2018</span> <br />
            </li>
            <li>
              <span className='title'>SOFTWARE ENGINEERING AND MANAGEMENT</span> <br />
              <span className='location'>University of Gothenburg, Gothenburg, Sweden</span> <br />
              <span className='date'>August 2017 - December 2017</span> <br />
            </li>
            <li>
              <span className='title'>HIGH SCHOOL DIPLOMA</span> <br />
              <span className='location'>First Language School, Varna, Bulgaria</span> <br />
              <span className='date'>September 2010 - May 2015</span> <br />
            </li>
          </ul>
        </div>
        <hr />
        <div className='work'>
          <TiBriefcase /> <h3>WORK EXPERIENCE</h3>
          <ul>
            <li>
              <span className='title'>ADVERTISING OPERATIONS</span> <br />
              <span className='location'>Titan Gate, Varna, Bulgaria</span> <br />
              <span className='date'>December 2017 - March 2018</span> <br />
            </li>
            <li>
              <span className='title'>LINK BUILDER</span> <br />
              <span className='location'>411 Marketing, Varna, Bulgaria</span> <br />
              <span className='date'>March 2017 - July 2017</span> <br />
            </li>
            <li>
              <span className='title'>CUSTOMER CARE REPRESENTATIVE</span> <br />
              <span className='location'>Sitel, Varna, Bulgaria</span> <br />
              <span className='date'>May 2016 - February 2017</span> <br />
            </li>
          </ul>
        </div>
        <hr />
      </div>
    </div>
  }
}

export default App;
